#include<cstring>
#include <iostream>

#define TAM_NOME 100
#define TAM_TIME 100
#define TAM_USER 201

class usuario
{
private:

	char nome_usuario[TAM_NOME];
	unsigned int id_usuario;
	char time_usuario[TAM_TIME];

public:

	usuario(const char nome[TAM_NOME],unsigned int id,const char time[TAM_TIME])
	{
		id_usuario = id;
		strncpy(nome_usuario,nome,TAM_NOME - 1);
		strncpy(time_usuario,time,TAM_TIME - 1);
	}

	 void imprime()
	 {
	 printf("-------------------------------\n");
	 printf("Usuário: %d\n%s\n%s\n",id_usuario,nome_usuario,time_usuario);
	 printf("-------------------------------\n");
	 }

	 usuario copia(usuario* to,usuario* from)
	 {

	 	strncpy(to->time_usuario,from->time_usuario,TAM_TIME - 1);
	 	to->id_usuario = from->id_usuario;
	 	strncpy(to->nome_usuario,from->nome_usuario,TAM_NOME - 1);

	 	return *to;

	 }

	 unsigned id()
	 {
		 int id = id_usuario;

		 return id;
	 }

	 usuario muda_id(usuario* u,unsigned int id2)
	 {
	 	u->id_usuario = id2;

	 	return *u;
	 }
};

class lista
{

private:

    usuario **vetor_usuarios;
    unsigned tamanho_lista;
    unsigned total_lista;

public:

    lista(unsigned tamanho,unsigned total, usuario** vetor)
	{
    	tamanho_lista = tamanho;
    	total_lista = total;
    	vetor_usuarios = vetor;

	}

    lista add_usuario(lista* lista,usuario* u)
    {
    	if(lista->tamanho_lista == lista->total_lista)
    	{
    		lista->total_lista += 5;
    		lista->vetor_usuarios = (usuario**) realloc(lista->vetor_usuarios,lista->total_lista*sizeof(usuario));
    	}
    	lista->vetor_usuarios[lista->tamanho_lista] = u;
    	++lista->tamanho_lista;

    	return *lista;
    }
    void imprime(lista* lista)
    {
    	for(unsigned i = 0; i < lista->tamanho_lista;i++)
    	{
    		lista->vetor_usuarios[i]->imprime();
    		//printf("%s\n",lista->vetor_usuarios[i]->nome);
    		//printf("%u\n",lista->vetor_usuarios[i]->id);
    		//printf("%s\n",lista->vetor_usuarios[i]->time);

    	}
    }

    usuario* procura_id(lista* lista,unsigned id)
    {
    	usuario* u;

    	for(unsigned i = 0; i < lista->tamanho_lista;++i)
    	{
    		if(lista->vetor_usuarios[i]->id() == id)
    		{
    			u = lista->vetor_usuarios[i];
    			return u;
    		}

    	}
    	printf("id não registrada!\n");

    	return NULL;
    }


    void remove_id(lista* lista,unsigned id)
    {
    	for(unsigned i = 0; i < lista->tamanho_lista;++i)
    	{
    		if(lista->vetor_usuarios[i]->id() == id)
    		{
    			//free(lista->vetor_usuarios[i]);
    			lista->vetor_usuarios[i] = lista->vetor_usuarios[lista->tamanho_lista - 1];
    			lista->vetor_usuarios[lista->tamanho_lista - 1] = NULL;
    			--lista->tamanho_lista;
    		}

    	}
    }


};





int main()
{
	//unsigned int x = 2;
	usuario** vetor = (usuario**) malloc(1*sizeof(usuario));
	usuario u1("Geraldo",1,"Tabajara");
	usuario u2("Goku",2,"Mengão");
	usuario* u3;
	lista lista1(0,1,vetor);

	lista1 = lista1.add_usuario(&lista1,&u1);
	lista1 = lista1.add_usuario(&lista1,&u2);

	//lista1.imprime(&lista1);

	//lista1.remove_id(&lista1,2);

	//lista1.imprime(&lista1);

	u3 = lista1.procura_id(&lista1,3);

	u3->imprime();

	//lista1.imprime(&lista1);

	//u2 = u2.copia(&u2,&u1);

	//u2.imprime();

	//u1 = u1.muda_id(&u1,x);

	//u1.imprime();

	return 0;
}
